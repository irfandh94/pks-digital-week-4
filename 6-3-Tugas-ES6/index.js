// 1

let  luas = (p,l) => { return p*l;};
let keliling = (p,l) => {return 2*p + 2*l;};
console.log('luas =',luas(10,5));
console.log('keliling =',keliling(10,5));


// No 2 

const newFunction = (firstName, lastName) =>
{ return {
      firstName,
      lastName,
      fullName: `${firstName}  ${lastName}`   
    }
  }
   
console.log(newFunction("William", "Imoh").fullName) 

// NO 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  const {firstName, lastName, address, hobby} = newObject
  console.log(firstName, lastName, address, hobby)

  // No 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]


//Driver Code
console.log(combined)

//No 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem  ${view} dolor sit amet, consectetur adipiscing elit, + ${planet} ` 