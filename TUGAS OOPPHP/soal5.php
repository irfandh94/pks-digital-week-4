<?php

class Hewan {
    protected $nama;

    public function __construct($nama) {

        $this->nama = $nama;
    }
}

class Komodo extends Hewan{

    public function getName()
    {
        return $this->nama;

    }
}

$komodo = new Komodo('komodo');
echo $komodo->getName();

?>