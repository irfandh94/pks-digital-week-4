<?php

namespace App\Http\Controllers;

use App\comments;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class CommentsController extends Controller
{
   
    public function index()
    {

       
        $comments = comments::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'list Comments',
            'data' => $comments
        ], 200);

    }

    public function show($id)
    {
        //find comments by ID
        $comments = comments::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data comments',
            'data'    => $comments 
        ], 200);

    }

    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = auth()->user();

        //save to database
        $comments = comments::create([
            'content'     => $request->content,
            'post_id' =>$request->post_id,
            'user_id' =>$request->user_id
        ]);
        dd($comments->post->users->email);
        Mail::to($comments->post->users->email)->send(new PostAuthorMail($comments));

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'comments Created',
                'data'    => $comments  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'comments Failed to Save',
        ], 409);

    }

    public function update(Request $request, comments $comments)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comments by ID
        $comments = comments::findOrFail($comments->id);

        if($comments) {

                
            
            //update comments
            $comments->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comments Updated',
                'data'    => $comments  
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comments by ID
        $comments = comments::findOrfail($id);

        if($comments) {

            $user = auth()->user();
            if($comments->user_id != $user->id){

                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden to Update',
                    'data'    => $post  
                ], 200);
    

                
            }

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comments Deleted',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments Not Found',
        ], 404);
    }

}
