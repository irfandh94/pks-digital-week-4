<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class users extends Authenticatable implements JWTSubject
{   
    use Notifiable;

    protected $fillable = ['username','email', 'name' , 'role_id' , 'password' , 'email_verified_at'];
    protected $hidden = [
      'password', 'remember_token',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {

        parent::boot();

    static::creating( function($model){
        if(empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()} = Str::uuid();
        }

        });
    }
    public function roles()
  {
    return $this->belongsTo('App\roles');
  }
  public function otp_code()
  {
    return $this->hasOne('App\OtpCode', 'user_id');
  }

  public function comments()
  {
      return $this->hasMany('App\comments');
  }

  public function post()
  {
      return $this->hasMany('App\Post');
  }

  
  
  public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}


