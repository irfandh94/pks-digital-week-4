<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('/post', 'PostController');
Route::apiResource('/comments', 'CommentsController');
Route::apiResource('/roles', 'RolesController');

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth'
] , function(){
    Route::post('register', 'registerController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update', 'UpdatePasswordController')->name('auth.update-password');
    Route::post('login', 'LoginController')->name('auth.login');

});
